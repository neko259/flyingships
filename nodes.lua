minetest.register_node("flyingships:frame_motor_directed", {
    description = "Flying Ship directed motor",
    tiles = {"flyingships_arrow.png^[transformR90", "flyingships_gear.png", "flyingships_gear.png",
             "flyingships_gear.png", "flyingships_gear.png", "flyingships_gear.png"},
    groups = {snappy=2,choppy=2,oddly_breakable_by_hand=2,mesecon=2,flyingship_motor=1},
    paramtype2 = "facedir",
    mesecons={effector={action_on=flyingships.directed_motor_on, action_off=flyingships.directed_motor_off}},
    on_rightclick = function(pos, node, clicker, itemstack, pointed_thing)
        if itemstack:get_name() == flyingships.CONFIGURATOR_NODE then
            flyingships.configure(pointed_thing, clicker)
        else
            local is_sneak = clicker:get_player_control().sneak
            flyingships.motor_on(pointed_thing, clicker, is_sneak)
        end
        return itemstack
    end,
    after_place_node = function(pos, placer, itemstack)
        local meta = minetest.get_meta(pos)
        meta:set_string("owner", placer:get_player_name())
    end
})

minetest.register_node("flyingships:frame_motor", {
    description = "Flying Ship key-only motor",
    tiles = {"flyingships_motor_key.png", "flyingships_motor_key.png", "flyingships_motor_key.png",
             "flyingships_motor_key.png", "flyingships_motor_key.png", "flyingships_motor_key.png"},
    groups = {snappy=2,choppy=2,oddly_breakable_by_hand=2,mesecon=2,flyingship_motor=1},
    paramtype2 = "facedir",
    on_rightclick = function(pos, node, clicker, itemstack, pointed_thing)
        if itemstack:get_name() == flyingships.CONFIGURATOR_NODE then
            flyingships.configure(pointed_thing, clicker)
        else
            local is_sneak = clicker:get_player_control().sneak
            flyingships.motor_on(pointed_thing, clicker, is_sneak)
        end
        return itemstack
    end,
    after_place_node = function(pos, placer, itemstack)
        local meta = minetest.get_meta(pos)
        meta:set_string("owner", placer:get_player_name())
    end
})

minetest.register_node("flyingships:frame_rotator", {
    description = "Flying Ship Rotator",
    tiles = {"flyingships_gear.png", "flyingships_gear.png", "flyingships_arrow.png^[transformFX",
             "flyingships_arrow.png^[transformFX", "flyingships_arrow.png^[transformFX",
             "flyingships_arrow.png^[transformFX"},
    groups = {snappy=2,choppy=2,oddly_breakable_by_hand=2,mesecon=2,flyingship_motor=1},
    paramtype2 = "facedir",
    on_rightclick = function(pos, node, clicker, itemstack, pointed_thing)
        if itemstack:get_name() == flyingships.CONFIGURATOR_NODE then
            flyingships.configure(pointed_thing, clicker)
        else
            flyingships.rotator_on(pointed_thing, clicker)
        end
        return itemstack
    end,
    after_place_node = function(pos, placer, itemstack)
        local meta = minetest.get_meta(pos)
        meta:set_string("owner", placer:get_player_name())
    end
})
